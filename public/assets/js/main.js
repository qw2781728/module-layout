$(".hover").mouseleave(
    function() {
        $(this).removeClass("hover");
    }
);
owlfun();
function owlfun(){
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: [
            "<i class='fa fa-caret-left'></i>",
            "<i class='fa fa-caret-right'></i>"
        ],
        autoplay: true,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
    }
// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
        $('#top').fadeIn("fast"); // Fade in the arrow
        $('#phone').fadeIn("fast");
        $('#faceb').fadeIn("fast");
        $('#comm').fadeIn("fast");
    } else {
        $('#top').fadeOut("fast"); // Else fade out the arrow
        $('#phone').fadeOut("fast");
        $('#faceb').fadeOut("fast");
        $('#comm').fadeOut("fast");        
    }
});
$('#top').click(function() { // When arrow is clicked
    $('body,html').animate({
        scrollTop: 0 // Scroll to top of body
    }, 500);
});


$(".tiny-img").click(function() {
    src = $(this).attr('src');
    $(".shown-img").attr('src', src);
});