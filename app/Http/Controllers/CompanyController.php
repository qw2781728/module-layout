<?php

namespace App\Http\Controllers;

use App\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::first();
        $company->description = str_replace("/upload", env('APP_BACK_URL').'upload', $company->description);
        return view('company.index', compact('company'));
    }
}
