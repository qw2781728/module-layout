<?php

namespace App\Http\Controllers;

use App\Company;
use App\Contact;
use App\Helpers\MailHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->company = Company::first();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = $this->company;
        return view('contact.index', compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request->all());
        $input = $request->all();
        unset($input['_token']);
        Contact::create($input);

        $message = "";
        $message .= "聯絡人：" . $request->name . "<br>";
        $message .= "連絡電話：" . $request->phone . "<br>";
        $message .= "所在地區：" . $request->area . "<br>";
        $message .= "需求金額：" . $request->money . "<br>";
        $message .= "是否有勞健保：" . $request->worker . "<br>";
        $message .= "額外訊息：" . $request->message . "<br>";

        $result = MailHelper::send($message, $this->company);
        Session::flash('contact',  ['type' => 'success', 'title' => $result]);
        return redirect()->back();    
    }

}
