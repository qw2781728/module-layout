<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = News::orderBy('sort');
        if (isset($request->tag)) {
            $query = $query->where('tag', 'like', '%'. $request->tag .'%');
        }
        $news = $query->get();
        $tags = array();
        foreach (News::orderBy('sort')->get() as $value) {
            $tags = array_merge($tags, explode(',', $value->tag));
        }
        $tags = array_unique($tags);
        return view('news.index', compact('news', 'tags')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {   
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {   
        $news->times += 1;
        $news->save();
        $news->content = str_replace("/upload", env('APP_BACK_URL').'upload', $news->content);
        return view('news.edit', compact('news'));
    }

}
