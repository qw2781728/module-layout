<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Service;
use App\Product;
use App\News;
use App\MetaSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::orderBy('sort')->get()->take(6);
        $products = Product::orderBy('sort')->get()->take(6);
        $customers = Customer::orderBy('sort')->get()->take(4);
        $news = News::orderBy('sort')->get()->take(4);

        return view('home.index', compact('services', 'products', 'customers', 'news')); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sitemap()
    {
        header('Content-type: application/xml');
        
        $pages = MetaSetting::pluck('page_type')->toArray();
        date_default_timezone_set('Asia/Taipei');
        $lastMod = date('Y-m-dCSTH:i:s').'+00:00';

        $xml = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

        foreach ($pages as $i => $type) {
            switch ($type) {
                case 'home':
                    $xml .= '
                        <url>
                            <loc>'.route('home').'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>1.00</priority>
                        </url>
                        <url>
                            <loc>'.route('contact').'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>0.9</priority>
                        </url>';
                    break;
                    
                case 'company':
                    $xml .= '
                        <url>
                            <loc>'.route('company').'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>0.9</priority>
                        </url>';
                    break;

                case 'news':
                    $xml .= '
                        <url>
                            <loc>'.route('news').'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>0.9</priority>
                        </url>';
                    foreach (News::all() as $news) {
                        $xml .= '
                        <url>
                            <loc>'.route('news.detail', $news).'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>0.9</priority>
                        </url>';
                    }
                    break;

                case 'product':
                    $xml .= '
                        <url>
                            <loc>'.route('product').'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>0.8</priority>
                        </url>';
                    foreach (Product::all() as $product) {
                        $xml .= '
                        <url>
                            <loc>'.route('product.detail', $product).'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>0.8</priority>
                        </url>';
                    }
                    break;

                case 'service':
                    foreach (Service::all() as $service) {
                        $xml .= '
                        <url>
                            <loc>'.route('service.detail', $service).'</loc>
                            <lastmod>'.$lastMod.'</lastmod>
                            <priority>0.9</priority>
                        </url>';
                    }
                    break;
            }
        }
        $xml .= '</urlset>';
        echo $xml;
        return;
    }
}
