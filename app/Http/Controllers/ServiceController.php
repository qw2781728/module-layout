<?php

namespace App\Http\Controllers;

use App\Service;

class ServiceController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $services = Service::orderBy('sort')->get();
        return view('service.edit', compact('service', 'services')); 
    }
}
