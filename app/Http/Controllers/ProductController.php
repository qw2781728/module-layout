<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Product::orderBy('sort');
        if (isset($request->tag)) {
            $query = $query->where('tag', 'like', '%'. $request->tag .'%');
        }
        $products = $query->get();
        $tags = array();
        foreach (Product::orderBy('sort')->get() as $product) {
            $tags = array_merge($tags, explode(',', $product->tag));
        }
        $tags = array_unique($tags);
        return view('product.index', compact('products', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product->content = str_replace("/upload", env('APP_BACK_URL').'upload', $product->content);
        return view('product.edit', compact('product')); 
    }
}
