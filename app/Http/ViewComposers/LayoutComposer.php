<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Company;
use App\MetaSetting;
use App\Repositories\MetaSettingRepository;

class LayoutComposer
{
    /**
     * The meta_setting repository implementation.
     *
     * @var MetaSettingRepository
     */
    protected $meta_setting;

    /**
     * Create a new layouts view composer.
     *
     * @param  MetaSettingRepository $meta_settings
     * @param  PromoteRepository $promotes
     * @return void
     */
    public function __construct(MetaSettingRepository $meta_setting)
    {
        // Dependencies automatically resolved by service container...
        $this->meta_setting = $meta_setting;
        $this->company = Company::first();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $meta_settings = MetaSetting::all();
        foreach ($meta_settings as $setting) {
            switch ($setting->page_type) {
                case 'home':
                    $setting->sort = 1;
                    break;
                case 'company':
                    $setting->sort = 2;
                    break;
                case 'news':
                    $setting->sort = 3;
                    break;
                case 'service':
                    $setting->sort = 4;
                    break;
                default:
                    $setting->sort = 5;
                    break;
            }
        }
        $meta_settings = $meta_settings->sortBy('sort')->values()->all();
        $view->with('meta_setting', $this->meta_setting->getMetaSetting());
        $view->with('meta_settings', $meta_settings);
        $view->with('company', $this->company);
    }
}