<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'logo', 'banner', 'fb_link', 'line_link', 'phone', 'shown_phone', 'email', 'address', 'map_link', 'map_iframe', 'description',
    ];
}
