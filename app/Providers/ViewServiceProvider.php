<?php

namespace App\Providers;

use App\MetaSetting;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Config;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {   
        $meta_settings = MetaSetting::all();
        foreach ($meta_settings as $setting) {
            switch ($setting->page_type) {
                case 'home':
                    $setting->sort = 1;
                    break;
                case 'company':
                    $setting->sort = 2;
                    break;
                case 'news':
                    $setting->sort = 3;
                    break;
                case 'service':
                    $setting->sort = 4;
                    break;
                default:
                    $setting->sort = 5;
                    break;
            }
        }
        $meta_settings = $meta_settings->sortBy('sort')->values()->all();
        View::share('meta_settings', collect($meta_settings));
        View::composer('layout.app', 'App\Http\ViewComposers\LayoutComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
