<?php
namespace App\Repositories;

use App\MetaSetting;
use App\News;
use App\Product;
use App\Service;
use Route;
use Request;

class MetaSettingRepository
{
    /**
     * Get the meta settings of the store by the type of the view.
     * 
     * @return array $meta_settings the meta settings of the store
     */
    public function getMetaSetting()
    {
        $routeName = Route::current()->getName();
        if (is_null($routeName)) { 
            $meta_setting = MetaSetting::where('page_type', 'home')->first();
        }
        else {
            $type = explode('.', $routeName)[0];
            $meta_setting = MetaSetting::where('page_type', $type)->first();
            if (is_null($meta_setting)) $meta_setting = MetaSetting::where('page_type', 'home')->first();
            switch ($routeName) {
                case 'news.detail':
                    $news = News::find(explode('/', Request::path())[1]);
                    if (!is_null($news)) $meta_setting->seo_title = $news->title;
                    break;
                case 'product.detail':
                    $product = Product::find(explode('/', Request::path())[1]);
                    if (!is_null($product)) {
                        $meta_setting->seo_title = $product->seo_title;
                        $meta_setting->seo_description = $product->seo_description;
                        $meta_setting->image = env('APP_BACK_URL').$product->firstPhoto();
                    }
                    break;
                case 'service.detail':
                    $service = Service::find(explode('/', Request::path())[1]);
                    if (!is_null($service)) {
                        $meta_setting->seo_title = $service->seo_title;
                        $meta_setting->seo_description = $service->seo_description;
                        $meta_setting->image = env('APP_BACK_URL').$service->img;
                    }
                    break;
            }
        }
        $meta_setting->image = isset($meta_setting->image) ? $meta_setting->image : asset('assets/img/banner.jpg');
        return $meta_setting;
    }
}