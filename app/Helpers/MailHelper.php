<?php

namespace App\Helpers;

require_once(__DIR__.'/lib/phpmailer/phpmailer/src/Exception.php');
require_once(__DIR__.'/lib/phpmailer/phpmailer/src/SMTP.php');
require_once(__DIR__.'/lib/phpmailer/phpmailer/src/PHPMailer.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MailHelper
{
    /**
     * Create a new mail.
     */
    public static function send($message, $company)
    {
        $mail = new PHPMailer();
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->IsSMTP();
        $mail->SMTPAuth = true;      
        $mail->Host = env('MAIL_HOST'); 
        $mail->Port = env('MAIL_PORT'); 
        $mail->CharSet = "utf-8";
        $mail->Encoding = "base64";
        $mail->Username = env('MAIL_USERNAME'); 
        $mail->Password = env('MAIL_PASSWORD'); 
        $mail->From = env('MAIL_USERNAME');
        $mail->FromName = "系統Mail"; 
        $mail->Subject = "網頁來信";  
        $mail->IsHTML(true);
        $mail->AddAddress($company->email, $company->title);  		
        $mail->Body = "這是網頁的來信，內容如下<br>" . $message; 
        $send = $mail->Send();
        $result = '訊息送出成功！';
        if(!$send) $result = $mail->ErrorInfo;
        $mail->ClearAddresses(); 
        
        return $result; 
    }
}
