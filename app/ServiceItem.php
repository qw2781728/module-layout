<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'service_id', 'title', 'sort', 'content'
    ];

    /**
     * Get the service for the service items.
     */
    public function service()
    {
        return $this->belongsTo('App\Service');
    }
}
