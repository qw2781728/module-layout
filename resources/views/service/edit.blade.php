@extends("layout.app")
@section('header')
<style>
    span.fa-check-square:before {
        content: "";
    }
</style>
@endsection
@section('content') 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $service->title }}</h3>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ $meta_settings->where('page_type', 'home')->first()['page_name'] }}</a></li>
                <li><a>{{ $meta_settings->where('page_type', 'service')->first()['page_name'] }}</a></li>
                <li class="active">{{ $service->title }}</li>
            </ol>
            <div class="row">
                <div class="col-md-12">
                    <nav class="col-md-3">
                        <ul class="nav sidebar-nav nav-pills nav-stacked">
                            @foreach ($services as $data)
                            <li class="mt0 {{ $data->id === $service->id ? 'active' : '' }}"><a href="{{ route('service.detail', $data) }}">{{ $data->title }}</a></li>
                            @endforeach
                        </ul>
                    </nav>
                    <div class="col-md-9">
                        @foreach ($service->serviceItems as $item)
                        <div class="sercontent">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="sercontenttitle">
                                        <h6>{{ $item->title }}</h6>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="sercontentcont">
                                    {!! str_replace("/upload", env('APP_BACK_URL').'upload', $item->content) !!}
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <a style="margin-left:8px;" class="btn btn-primary pull-left" href="{{ route('contact') }}">立即填寫聯絡表單</a>
                    </div> 
                </div>
            </div>
        </div>
    </section><br>
@endsection
@section('scripts')

@endsection
