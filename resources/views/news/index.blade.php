@extends("layout.app")
@section('header')
   
@endsection
@section('content') 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $meta_settings->where('page_type', 'news')->first()['page_name'] }}</h3>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ $meta_settings->where('page_type', 'home')->first()['page_name'] }}</a></li>
                <li class="active"><a href="{{ route('news') }}">{{ $meta_settings->where('page_type', 'news')->first()['page_name'] }}</a></li>
            </ol>
            <div class="pagecontent">
                <div class="row">
                    <nav class="col-md-3">
                        <ul class="nav sidebar-nav nav-pills nav-stacked">
                            @foreach ($tags as $tag)
                            <li class="{{ (str_replace('news/', '', Request::path()) == $tag) ? 'active' : '' }}"><a href="{{ route('news').'?tag='.$tag }}">{{ $tag }}</a></li>
                            @endforeach
                        </ul>
                    </nav>
                    <div class="col-md-9">
                        @foreach ($news as $data)    
                        <div class="col-md-4 text-center" style="text-align: -webkit-center;margin:20px 0;">
                            <a href="{{ route('news.detail', $data) }}">
                                <img style="height:160px; margin-bottom: 20px;" src="{{ env('APP_BACK_URL').$data->img }}" alt="" class="img-responsive">
                                {{ $data->title }}
                            </a>
                        </div>
                        @endforeach
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
       
    </script>
@endsection
