@extends("layout.app")
@section('header')

@endsection
@section('content') 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $meta_settings->where('page_type', 'news')->first()['page_name'] }}</h3>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ $meta_settings->where('page_type', 'home')->first()['page_name'] }}</a></li>
                <li><a href="{{ route('news') }}">{{ $meta_settings->where('page_type', 'news')->first()['page_name'] }}</a></li>
                <li class="active"><a href="javascript:;">{{ $news->title }}</a></li>
            </ol>
            <div class="pagecontent">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <div class="">
                            <h5>{{ $news->title }}</h5><span>{{ $news->updated_at }}</span>
                            <img style="margin:1rem 0;" class="img-responsive" src="{{ env('APP_BACK_URL').$news->img }}"  alt="{{ $news->title }}" width="380" height="300" />
                            
                        </div>
                    </div>
                    <div class="col-md-8" style="word-break: break-all;">
                        {!! $news->content !!}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="container text-center">
                        @foreach (explode(',', $news->tag) as $tag)
                        <a style="margin-top: 5px; margin-bottom: 5px;" class="btn btn-primary" title="{{ $tag }}" href="{{ route('news').'?tag='.$tag }}">#{{ $tag }}</a>
                        @endforeach
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        
    </script>
@endsection
