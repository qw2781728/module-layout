@extends("layout.app")
@section('content') 
    <header id="header" class="">
        <div class="container">
            <div class="row mgT9">
                <div class="col-md-6 text-center">
                    <h1 class="font-size-30" style="color:#000000; font-size: 55px;"><b>台中當舖｜兆豐當舖</b></h1>
                    <h2 class="font-size-30">汽機車借款、免留車</h2>
                    <h2 class="font-size-30" style="color:#000000;"><b>當日快速撥款</b></h2>
                    <h3 class="font-size-30"><b>04-22355111</b></h3>
                    <a id="scrolla" href="#service"><span></span><!-- Scroll --></a>
                </div>
                <br>
                <div class="col-md-4 col-md-offset-2" id="contact">
                {{ Form::open(['method' => 'POST', 'action' => 'ContactController@store']) }}
                    <h2 class="text-center contact-title">聯絡我們</h2>
                    <div class="login-form" style="background-color: #b0e5ff;">
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="name" placeholder="輸入您的姓名" required>
                            <label class="login-field-icon fui-user"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="phone" placeholder="輸入您的手機" required>
                            <label class="login-field-icon fui-mic"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="area" placeholder="輸入您的所在地區" required>
                            <label class="login-field-icon fui-mail"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="money" placeholder="輸入您的需求金額" required>
                            <label class="login-field-icon fui-mail"></label>
                        </div>
                        <div class="form-group">
                            <select class="form-control login-field" name="worker" required>
                                <option>-- 是否有勞健保 --</option>
                                <option value="是">是</option>
                                <option value="否">否</option>
                            </select>
                            <label class="login-field-icon fui-home"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="message" placeholder="輸入您想要留下的額外訊息">
                            <label class="login-field-icon fui-new"></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">送出表單</button>
                    </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </header>
    
    <section id="service">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>{{ $meta_settings->where('page_type', 'service')->first()['page_name'] }}</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="row">
                        @foreach ($services as $key => $service)
                        <div class="col-md-4">
                            <div class="servecard">
                                <figure class="snip1190 content-card">
                                    <img class="img-responsive" src="{{ env('APP_BACK_URL').$service->img }}" style="height: 100%;" />
                                    <figcaption>
                                        <div class="square">
                                            <div></div>
                                        </div>
                                        <h2>{{ $service->title }}</h2>
                                    </figcaption>
                                    <a href="{{ route('service.detail', $service) }}" title="{{ $service->title }}"></a>
                                </figure>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section id="news">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sectiontitle">
                                    <h4>客戶分享</h4>
                                </div>
                                <div class="row" id="sharediv" style="padding:0 15px;">
                                    @foreach ($customers as $customer)
                                    <div class="newsshare">                   
                                        <hr>                                  
                                        <h6>{{ $customer->title }}－{{ $customer->content }}</h6>                 
                                        <span>時間:{{ $customer->updated_at }}</span>
                                    </div>      
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="hr">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sectiontitle">
                                    <h4>{{ $meta_settings->where('page_type', 'news')->first()['page_name'] }}</h4>
                                </div>
                                <div class="row" id="newsdiv">
                                    @foreach ($news as $data)                                                               
                                    <div class="row" style="padding:0 15px;">                                                                
                                        <hr>                                                                         
                                        <div class="col-md-6">                                                           
                                            <a href="{{ route('news.detail', $data) }}" title="{{ $data->title }}"><img class="img-responsive" src="{{ env('APP_BACK_URL').$data->img }}"></a>    
                                        </div>                                                                       
                                        <div class="col-md-6">                                                           
                                            <h5><a href="{{ route('news.detail', $data) }}" title="{{ $data->title }}">{{ $data->title }}</a></h5>    
                                        </div>                                                                       
                                        <hr>                                                                     
                                    </div>  
                                    @endforeach  
                                    <a class="pull-right btn btn-primary" href="{{ route('news') }}" target="_blank" rel="noopener noreferrer">觀看更多</a>                                                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="prod">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $meta_settings->where('page_type', 'product')->first()['page_name'] }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="carousel-wrap">
                    <div class="owl-carousel" id="shopdiv">
                        @foreach ($products as $product)
                        <div class="item">
                            <figure class="snip1206">
                                <img src="{{ $product->firstPhoto() }}" alt="{{ $product->title }}" />
                                <figcaption>
                                    <h4>{{ $product->title }}</h4>
                                </figcaption>
                                <a href="{{ route('product.detail', $product) }}"></a>
                            </figure>
                        </div>
                        @endforeach    
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'contact',
        ]])
    </script>
@endsection
