@extends("layout.app")
@section('content') 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>聯絡我們</h3>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ $meta_settings->where('page_type', 'home')->first()['page_name'] }}</a></li>
                <li class="active">聯絡我們</li>
            </ol>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 text-center">
                        {!! $company->map_iframe !!}
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                        <h4>{{ $company->title }}</h4>
                        <p>電話：{{ $company->shown_phone }}</p>
                        <p>地址：{{ $company->address }}</p>
                        <p>信箱：{{ $company->email }}</p>
                        <p>LineID：{{ $company->line_link }}</p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row inputrow">
                <div class="col-md-12">
                    {{ Form::open(['method' => 'POST', 'action' => 'ContactController@store']) }}
                    <div class="login-form">
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="name" placeholder="輸入您的姓名" required>
                            <label class="login-field-icon fui-user"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="phone" placeholder="輸入您的手機" required>
                            <label class="login-field-icon fui-mic"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="area" placeholder="輸入您的所在地區" required>
                            <label class="login-field-icon fui-mail"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="money" placeholder="輸入您的需求金額" required>
                            <label class="login-field-icon fui-mail"></label>
                        </div>
                        <div class="form-group">
                            <select class="form-control login-field" name="worker" required>
                                <option>-- 是否有勞健保 --</option>
                                <option value="是">是</option>
                                <option value="否">否</option>
                            </select>
                            <label class="login-field-icon fui-home"></label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control login-field" name="message" placeholder="輸入您想要留下的額外訊息">
                            <label class="login-field-icon fui-new"></label>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-8">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">送出表單</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        @include('session_flash', ['flashes' => [
            'contact',
        ]])
    </script>
@endsection
