@extends("layout.app")
@section('content') 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $meta_settings->where('page_type', 'company')->first()['page_name'] }}</h3>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ $meta_settings->where('page_type', 'home')->first()['page_name'] }}</a></li>
                <li class="active">{{ $meta_settings->where('page_type', 'company')->first()['page_name'] }}</li>
            </ol>
            <div class="pagecontent">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <div class="">
                           <img class="img-responsive" src="{{ asset('assets/img/logo.png') }}"  alt="小額借貸" width="380" height="300" />
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="contentcont">
                        {!! $company->description !!}
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        
    </script>
@endsection
