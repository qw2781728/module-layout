<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="author">
    <link rel="icon" href="{{ asset('/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="title" content="{{ $meta_setting->seo_title }}">
    <meta name="description" content="{{ $meta_setting->seo_description }}">
    <title>{{ $meta_setting->seo_title }}</title>
    <meta property="og:url" content="{{ env('APP_URL') }}" />
    <meta property="og:title" content="{{ $meta_setting->seo_title }}" />
    <meta property="og:description" content="{{ $meta_setting->seo_description }}">
    <meta property="og:image" content="{{ $meta_setting->image }}" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.3.0/css/flat-ui.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/bestcol.css') }}" rel="stylesheet">
    <style>
        .mgT9 {
            margin-top:9rem;
        }
        .hr {
            display: none;
        }
        @media screen and (max-width: 980px) {
            .mgT9 {
                margin-top:auto;
            }
        }
        @media screen and (max-width: 768px) {
            #header {
                height: 145vh;
                background-size: cover;
            }
            .font-size-30 {
                font-size: 30px !important;
            }
            .contact-title {
                color: #fff !important;
                text-shadow: 0 0 20px rgba(0,0,0,0.5);
            }
            .hr {
                display: block;
                border-top: 1px solid #fff;
            }
        }
        .tiny-img {
            border:1px solid #fff;
            border-radius:5px;
            width: 40px; 
            height: 40px;
            display: inline-block;
            cursor: pointer;
        }
        .shown-img {
            border:1px solid #fff;
            border-radius:5px;
            width: 300px; 
            height: 300px;
        }
        iframe {
            max-width: 100% !important;
        }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144575624-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-144575624-1');
    </script> -->
    @yield("header")
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/"><img class="img-responsive logo" src="{{ asset('assets/img/logo.png') }}"  alt="小額借貸" width="225" height="165" /></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse topLeftBorders ">
                <ul class="nav navbar-nav navbar-right">
                    @foreach($meta_settings as $setting)
                    <li><a href="{{ $setting->page_type !== 'service' ? route($setting->page_type) : '/#service' }}">{{ $setting->page_name }}</a></li>
                    @endforeach
                    <li><a href="{{ route('contact') }}">聯絡我們</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <a href="javascript:" id="top"><i class="fa fa-arrow-up"></i></a>
    <a href="tel:{{ $company->phone }}" id="phone"><i class="fa fa-volume-control-phone"></i></a>
    <a href="{{ $company->fb_link }}" id="faceb"><i class="fa fa-facebook"></i></a>
    <a href="https://line.me/ti/p/~{{ $company->line_link }}" id="comm" style="background: #20E000;"><i class="fa fa-commenting-o"></i></a>
    <section class="content">
        @yield('content')
    </section>
    <footer id="footer" style="background-color:#ffffff;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3 footertt">
                        <h4></h4>
                        <p>電話: <a href="tel:{{ $company->phone }}">{{ $company->shown_phone }}</a></p>
                        <p>信箱: <a href="mailto:{{ $company->email }}">{{ $company->email }}</a></p>
                        <p>地址: <a href="{{ $company->map_link }}">{{ $company->address }}</a></p>
                    </div>
                    <div class="col-md-7">
                        <div class="sectiontitle text-center">
                            <h4>服務專線<span class="hidden-xs">:</span><span class="visible-xs"><br></span><a href="tel:{{ $company->phone }}">{{ $company->shown_phone }}</a></h4>
                        </div>
                        <div class="sectiontitle text-center">
                            {!! $company->map_iframe !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-10 col-xs-6" style="margin:10px 0;">
                                <a href="{{ $company->fb_link }}">
                                    <img class="img-responsive" src="{{ asset('assets/img/fb.png') }}" alt="">
                                </a>
                            </div>
                            <div class="col-md-10 col-xs-6" style="margin:10px 0;">
                                <a href="https://line.me/ti/p/~{{ $company->line_link }}">
                                    <img class="img-responsive" src="{{ asset('assets/img/line.png') }}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="https://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.12.11/dist/sweetalert2.all.js"></script>
    <script>
    $(function() {
        $('#scrolla').on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top + 70 }, 600, 'linear');
        });
    });
    </script>
    @yield('scripts')
</body>

</html>
