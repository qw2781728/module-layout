@foreach ($flashes as $flash)
    @if (in_array($flash, array_keys(Session::all())))
        swal({
            type: "{{ session($flash)['type'] }}",
            title: "{{ session($flash)['title'] }}",
            showConfirmButton: false,
            timer: 1500,
        });
        @break
    @endif
@endforeach