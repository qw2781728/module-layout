@extends("layout.app")
@section('header')
@endsection
@section('content') 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $meta_settings->where('page_type', 'product')->first()['page_name'] }}</h3>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ $meta_settings->where('page_type', 'home')->first()['page_name'] }}</a></li>
                <li><a href="{{ route('product') }}">{{ $meta_settings->where('page_type', 'product')->first()['page_name'] }}</a></li>
                <li class="active"><a href="javascript:;">{{ $product->title }}</a></li>
            </ol>
            <div class="pagecontent">
                <div class="row">
                    <div class="col-md-6 col-md-offset-1">
                        <div class="col-md-12" style="margin:1rem 0;">
                            <img class="img-responsive shown-img" src="{{ $product->firstPhoto() }}" alt="{{ $product->title }}"/>
                        </div>
                        <div class="col-md-12">
                            @foreach (json_decode($product->img, true) as $image)
                            <img class="img-responsive tiny-img" src="{{ env('APP_BACK_URL').$image }}" />
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-12">
                            <h5>{{ $product->title }}</h5>
                        </div>
                        <div class="col-md-6">
                            @if (!is_null($product->special_price))
                            <h5><s>{{ $product->price }}</s></h5>
                            @else
                            <h5>{{ $product->price }}</h5>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <h5>{{ $product->special_price }}</h5>
                        </div>
                        <div class="col-md-12">
                            <label>分類：
                                @foreach (explode(',', $product->tag) as $tag)
                                <a class="btn btn-primary" title="{{ $tag }}" href="{{ route('product').'?tag='.$tag }}">#{{ $tag }}</a>
                                @endforeach 
                            </label>
                        </div>
                        
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1">
                        <label>介紹：
                            {!! $product->content !!}
                        </label>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        
    </script>
@endsection
