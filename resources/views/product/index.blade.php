@extends("layout.app")
@section('content') 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>{{ $meta_settings->where('page_type', 'product')->first()['page_name'] }}</h3>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">{{ $meta_settings->where('page_type', 'home')->first()['page_name'] }}</a></li>
                <li class="active"><a href="{{ route('product') }}">{{ $meta_settings->where('page_type', 'product')->first()['page_name'] }}</a></li>
            </ol>
            <div class="pagecontent">
                <div class="row">
                    <nav class="col-md-3">
                        <ul class="nav sidebar-nav nav-pills nav-stacked">
                            @foreach ($tags as $tag)
                            <li class="{{ (str_replace('product/', '', Request::path()) == $tag) ? 'active' : '' }}"><a href="{{ route('product').'?tag='.$tag }}">{{ $tag }}</a></li>
                            @endforeach
                        </ul>
                    </nav>
                    <div class="col-md-9">
                        @foreach ($products as $product)
                        <div class="col-md-4 text-center col-xs-6" style="text-align: -webkit-center;margin:20px 0;">
                            <a href="{{ route('product.detail', $product) }}">
                                <img style="height:160px; margin-bottom: 20px;" src="{{ $product->firstPhoto() }}" alt="" class="img-responsive">
                                {{ $product->title }}
                            </a>
                        </div>
                        @endforeach
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
@endsection
