<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('/', 'HomeController');
Route::get('/', 'HomeController@index')->name('home');

Route::resource('/company', 'CompanyController');
Route::get('/company', 'CompanyController@index')->name('company');

Route::resource('/news', 'NewsController');
Route::get('/news', 'NewsController@index')->name('news');
Route::get('/news/{news}/detail', 'NewsController@edit')->name('news.detail');

Route::resource('/product', 'ProductController');
Route::get('/product', 'ProductController@index')->name('product');
Route::get('/product/{product}/detail', 'ProductController@edit')->name('product.detail');

Route::resource('/service', 'ServiceController');
Route::get('/service/{service}/detail', 'ServiceController@edit')->name('service.detail');

Route::resource('/contact', 'ContactController');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/contact', 'ContactController@store')->name('contact.store');

Route::get('/sitemap.xml', 'HomeController@sitemap')->name('sitemap');

